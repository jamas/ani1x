from argparse import ArgumentParser
import logging
import os
import schnetpack as spk
import ase

from ani1x import ANI1x


def main(db_path):
    assert args.path.endswith('.db') ,"path must end with .db"
    ani1x = ANI1x(db_path)

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--db_path")
    args = parser.parse_args()
    logging.basicConfig(level=logging.INFO)

    main(args.db_path)
