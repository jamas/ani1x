import logging
import os
import shutil
import tarfile
import tempfile
from urllib import request as request

import h5py
import numpy as np
import ase
from ase.db import connect
from ase.units import Hartree

import schnetpack as spk
from schnetpack.datasets import DownloadableAtomsData


class ANI1x(DownloadableAtomsData):
    """ANI1x benchmark database.

    This class adds convenience functions to download ANI1 from figshare and
    load the data into pytorch.

    Args:

        dbpath (str): path to directory containing database.
        download (bool, optional): enable downloading if database does not exists.
        subset (list, optional): indices to subset. Set to None for entire database.
        load_only (list, optional): reduced set of properties to be loaded
        collect_triples (bool, optional): Set to True if angular features are needed.
        num_heavy_atoms (int, optional): number of heavy atoms.
            (See 'Table 1' in Ref. [#ani1]_)
        high_energies (bool, optional): add high energy conformations.
            (See 'Technical Validation' of Ref. [#ani1]_)
        environment_provider (spk.environment.BaseEnvironmentProvider): define how
            neighborhood is calculated
            (default=spk.environment.SimpleEnvironmentProvider).

    References:
        .. [#ani1] https://arxiv.org/abs/1708.04987

    """

    # properties:
    energy = "wb97x_dz.energy"
    forces = "wb97x_dz.forces"

    # reference = {energy: 0}

    # self_energies = {
    #     "H": -0.500607632585,
    #     "C": -37.8302333826,
    #     "N": -54.5680045287,
    #     "OX": -75.0362229210,
    # }

    def __init__(
        self,
        dbpath,
        download=True,
        subset=None,
        load_only=None,
        collect_triples=False,
        num_heavy_atoms=8,
        high_energies=False,
        environment_provider=spk.environment.SimpleEnvironmentProvider(),
    ):
        available_properties = [ANI1x.energy, ANI1x.forces]
        units = [Hartree]

        self.num_heavy_atoms = num_heavy_atoms
        self.high_energies = high_energies

        super().__init__(
            dbpath=dbpath,
            subset=subset,
            download=download,
            load_only=load_only,
            collect_triples=collect_triples,
            available_properties=available_properties,
            units=units,
            environment_provider=environment_provider,
        )

    def create_subset(self, idx):
        """Return a new database that only consists of provided indices.

        Args:
            idx (numpy.ndarray): indices to subset.

        Returns:
            schnetpack.data.AtomsData: database with subset of original data.

        """
        idx = np.array(idx)
        subidx = idx if self.subset is None else np.array(self.subset)[idx]

        return type(self)(
            dbpath=self.dbpath,
            download=False,
            subset=subidx,
            load_only=self.load_only,
            collect_triples=self.collect_triples,
            num_heavy_atoms=self.num_heavy_atoms,
            high_energies=self.high_energies,
            environment_provider=self.environment_provider,
        )

    def _download(self):
        tmpdir = tempfile.mkdtemp("ani1x")
        h5_path = self._download_raw(tmpdir)
        self._process_raw(h5_path)
        shutil.rmtree(tmpdir)

        # atref, labels = self._create_atoms_ref()

        # self.set_metadata({"atomrefs": atref.tolist(), "atref_labels": labels})


    def _download_raw(self, tmpdir):
        logging.info("downloading ANI-1x data...")
        h5_path = os.path.join(tmpdir, "ANI1x_release.h5")
        url = "https://ndownloader.figshare.com/files/18112775"
        request.urlretrieve(url, h5_path)

        return h5_path


    def _process_raw(self, h5_path):
        logging.info("processing h5 data")
        keys = [ANI1x.energy, ANI1x.forces]

        with ase.db.connect(self.dbpath, append=False) as asedb:
            for data in _iter_data_buckets(h5_path, keys):
                atomic_numbers = data["atomic_numbers"]
                coordinates = data["coordinates"]

                featuress = [data[key] for key in keys]

                for coordinate, *features in zip(coordinates, *featuress):
                    data = {key: feature for key, feature in zip(keys, features)}
                    atoms = ase.Atoms(atomic_numbers, coordinate)
                    asedb.write(atoms, data=data)


    def _create_atoms_ref(self):
        atref = np.zeros((100, 6))
        labels = self.load_only

        # converts units to eV (which are set to one in ase)
        atref[1, :] = self.self_energies["H"] * self.units["energy"]
        atref[6, :] = self.self_energies["C"] * self.units["energy"]
        atref[7, :] = self.self_energies["N"] * self.units["energy"]
        atref[8, :] = self.self_energies["O"] * self.units["energy"]

        return atref, labels


# data_keys =  # Original ANI-1x data (https://doi.org/10.1063/1.5023802)
def _iter_data_buckets(h5filename, keys):
    """Iterate over buckets of data in ANI HDF5 file.
    Yields dicts with atomic numbers (shape [Na,]) coordinated (shape [Nc, Na, 3])
    and other available properties specified by `keys` list, w/o NaN values.
    """
    keys = set(keys)
    keys.discard("atomic_numbers")
    keys.discard("coordinates")
    with h5py.File(h5filename, "r") as f:
        for grp in f.values():
            Nc = grp["coordinates"].shape[0]
            mask = np.ones(Nc, dtype=np.bool)
            data = dict((k, grp[k][()]) for k in keys)
            for k in keys:
                v = data[k].reshape(Nc, -1)
                mask = mask & ~np.isnan(v).any(axis=1)
            if not np.sum(mask):
                continue
            d = dict((k, data[k][mask]) for k in keys)
            d["atomic_numbers"] = grp["atomic_numbers"][()]
            d["coordinates"] = grp["coordinates"][()][mask]
            yield d
