to install requirements

pip install -r requirements.txt

To download dataset run

python get_dataset.py --path path/to/download/database.db

to access force and energy use the keys

    energy: wb97x_dz.energy
    forces: wb97x_dz.forces

